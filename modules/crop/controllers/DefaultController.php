<?php

namespace app\modules\crop\controllers;

use app\modules\crop\models\files\Files;
use app\modules\crop\models\files\ResizeForm;
use app\modules\crop\models\users\Users;
use yii\web\Controller;
use yii\web\UploadedFile;

class DefaultController extends Controller
{
    public $layout = false;

    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new Files();

        $model_resize = new ResizeForm();

        if (\Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->upload()) {

                $model->save();
                // file is uploaded successfully
            }
        }

        $user = Users::find()->where([
            'ip' => $_SERVER['REMOTE_ADDR']
        ])->one();

        return $this->render('index.twig', [
            'files' => $user->files,
            'model' => $model,
            'model_resize' => $model_resize,
        ]);
    }

    /**
     * @param $id
     * @param $width
     * @param $height
     */
    public function actionResize($id, $width, $height)
    {
        var_dump(1);die();
        $file = Files::findOne($id);

        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg(\Yii::getAlias('@webroot') . '/' . Files::PATH_ORIGINAL . $file->file);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $file->getOriginalWidth(), $file->getOriginalHeight());

        if ($file->getOriginalMime() == Files::MIME_JPEG)
            imagejpeg($image_p, Files::PATH_RESIZE . $file->file, 100);
    }
}
