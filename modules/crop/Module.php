<?php

namespace app\modules\crop;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\crop\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
