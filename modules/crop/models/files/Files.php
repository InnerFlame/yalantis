<?php

namespace app\modules\crop\models\files;

use app\modules\crop\models\users\Users;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "files".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $file
 * @property string $details_original
 * @property string $details_resize
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Users $user
 */
class Files extends \yii\db\ActiveRecord
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    const MIME_GIF = 'image/gif';
    const MIME_JPEG = 'image/jpeg';
    const MIME_PJPEG = 'image/pjpeg';
    const MIME_PNG = 'image/png';
    const MIME_SVG = 'image/svg+xml';

    const PATH_ORIGINAL = 'uploads/original/';
    const PATH_RESIZE = 'uploads/resize/';

    public $info = [];

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['file'], 'file', 'extensions' => 'gif, jpg'],


//            [['user_id', 'file'], 'required'],
//            [['user_id', 'created_at', 'updated_at'], 'integer'],
//            [['details_original', 'details_resize'], 'string'],
//            [['file'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('app', 'ID'),
            'user_id'          => Yii::t('app', 'User ID'),
            'file'             => Yii::t('app', 'File'),
            'details_original' => Yii::t('app', 'Details Original'),
            'details_resize'   => Yii::t('app', 'Details Resize'),
            'created_at'       => Yii::t('app', 'Created At'),
            'updated_at'       => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $user = Users::find()->where([
                'ip' => $_SERVER['REMOTE_ADDR']
            ])->one();

            if (!is_object($user)) {
                $user = new Users();
                $user->ip = $_SERVER['REMOTE_ADDR'];
                $user->save();
            }

            $this->user_id = $user->id;

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     * @return FilesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FilesQuery(get_called_class());
    }

    /*
    |--------------------------------------------------------------------------
    | Model relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /*
     |--------------------------------------------------------------------------
     | Methods
     |--------------------------------------------------------------------------
     */

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {

            $this->file->saveAs($this->setPath());

            $this->setInfo($this->setPath());

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function setPath()
    {
        return self::PATH_ORIGINAL . $this->file->baseName . '.' . $this->file->extension;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return  '/' . self::PATH_ORIGINAL  . $this->file;;
    }

    /**
     * @param $path
     */
    public function setInfo($path)
    {
       $this->details_original = json_encode([
           'info' => getimagesize($path)
       ]);
    }

    /**
     * @return mixed
     */
    public function getOriginalWidth()
    {
        $data = json_decode($this->details_original, true);

        return $data['info'][0];
    }

    /**
     * @return mixed
     */
    public function getOriginalHeight()
    {
        $data = json_decode($this->details_original, true);

        return $data['info'][1];
    }

    /**
     * @return mixed
     */
    public function getOriginalMime()
    {
        $data = json_decode($this->details_original, true);

        return $data['info']['mime'];
    }

}
