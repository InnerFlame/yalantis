<?php

namespace app\modules\crop\models\files;

use Yii;

class ResizeForm extends Files
{
    /*
    |--------------------------------------------------------------------------
    | Constants && properties
    |--------------------------------------------------------------------------
    */

    public $id;

    public $width;

    public $height;

    /*
    |--------------------------------------------------------------------------
    | Model configurations
    |--------------------------------------------------------------------------
    */

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['width', 'height'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'width'  => Yii::t('app', 'width'),
            'height' => Yii::t('app', 'height'),
        ];
    }

    /*
     |--------------------------------------------------------------------------
     | Methods
     |--------------------------------------------------------------------------
     */

}
