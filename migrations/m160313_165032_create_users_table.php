<?php

use yii\db\Migration;

class m160313_165032_create_users_table extends Migration
{
    const TABLE_NAME = 'users';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'ip'         => $this->string(30)->notNull(),
            'info'       => $this->text()->defaultValue(null),
            'created_at' => $this->integer(11)->defaultValue(null),
            'updated_at' => $this->integer(11)->defaultValue(null),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
