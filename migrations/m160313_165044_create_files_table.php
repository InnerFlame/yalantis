<?php

use yii\db\Migration;

class m160313_165044_create_files_table extends Migration
{
    const TABLE_NAME = 'files';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'               => $this->primaryKey(),
            'user_id'          => $this->integer(11)->notNull(),
            'file'             => $this->string(100)->notNull(),
            'details_original' => $this->text()->defaultValue(null),
            'details_resize'   => $this->text()->defaultValue(null),
            'created_at'       => $this->integer(11)->defaultValue(null),
            'updated_at'       => $this->integer(11)->defaultValue(null),
        ]);


        $this->createIndex('user_id', self::TABLE_NAME, 'user_id', false);

        $this->addForeignKey('fk_files_user_id', self::TABLE_NAME, 'user_id', m160313_165032_create_users_table::TABLE_NAME, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_files_user_id', self::TABLE_NAME);

        $this->dropIndex('user_id', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
