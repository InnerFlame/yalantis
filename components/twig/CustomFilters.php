<?php
/**
 * Created by PhpStorm.
 * @author: Roman Budnitsky
 * @date: 05.11.14 16:14
 * @copyright Copyright (c) 2014 PFSOFT LLC
 *
 * Class AccessFilter
 * @package app\components\twig
 */


namespace app\components\twig;

use app\components\ceo\CeoHelper;
use app\components\ceo\CeoWidget;
use app\components\Entity;
use app\components\langInput\LangInput;
use app\components\langInput\LangInputBehavior;
use app\components\langInput\LangInputWidget;
use app\models\ceo\Ceo;
use app\models\community\Likes;
use app\models\forms\BugReportForm;
use app\models\forms\FeedbackForm;
use app\models\forms\FeedbackFormButtom;
use app\models\forms\GeneralSettingForm;
use app\modules\forum\models\FrmComment;
use app\models\lang\Languages;
use app\models\main\Pages;
use app\models\rule\RuleAlias;
use app\models\user\User;
use yii;
use yii\helpers\HtmlPurifier;
use kartik\date\DatePicker;
use dosamigos\multiselect\MultiSelect;
use kartik\select2\Select2;
use app\components\MultiSelectCustom;
use yii\widgets\ListView;

class CustomFilters extends \Twig_Extension
{

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            //'preg_replace' => new \Twig_Filter_Method($this, 'pregReplace'),
        );
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
//            new \Twig_SimpleFunction('is_dir', array($this, 'is_dir')),
//            new \Twig_SimpleFunction('get_file_info', array($this, 'get_file_info')),
        );
    }

    /**
     * @param string $content
     * @return string
     */
    public function getHtmlPurifier($content)
    {
        return HtmlPurifier::process($content);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'custom_filters';
    }

//    /**
//     * @param $file
//     * @return bool
//     */
//    function is_dir($file)
//    {
//        return is_dir($file);
//    }
//
//    /**
//     * @param $path
//     * @return array
//     */
//    function get_file_info($path)
//    {
//        return getimagesize('uploads/' . $path);
//    }
}