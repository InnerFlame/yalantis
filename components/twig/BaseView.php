<?php

/*
 * This file is part of Twig.
 *
 * (c) 2011 Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace app\components\twig;

use yii\helpers\ArrayHelper;

class BaseView extends \Twig_Extension
{
    /**
     * @return array An array of global functions
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('setView', 'app\components\twig\set_view_params', array('needs_context' => true)),
        );
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'view';
    }
}

function set_view_params($context)
{
    ob_start();

    foreach($context as $key => $val) {
        if(substr($key, 0, 1) == '_') {
            $tmp_key = substr($key, 1);
            $tmp_keys = explode('_', $tmp_key);
            if(count($tmp_keys) > 1) {
                if(!is_object($context['this']->$tmp_keys[0])) {
                    $val = [$tmp_keys[1] => $val];
                    $context['this']->$tmp_keys[0] = ArrayHelper::merge($context['this']->$tmp_keys[0], $val);
                } else {
                    $context['this']->$tmp_keys[0]->$tmp_keys[1] = $val;
                }
            } else {
                $context['this']->$tmp_keys[0] = $val;
            }
        }
        //unset($context[$key]);
    }

    return ob_get_clean();
}
